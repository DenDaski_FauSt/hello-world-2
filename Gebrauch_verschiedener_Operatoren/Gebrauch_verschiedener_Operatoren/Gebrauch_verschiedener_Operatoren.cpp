#include<iostream>
using namespace std;

int main()
{
	int a = 2;
	int b = 1;

	a = b = 2; // b wird der Wert 2 zugewiesen und dann wird a der Wert von b zugewiesen
	a = 5 * 3 + 2; // Zuerst Multiplikation und dann Addiotion wie in der Mathe
	a = 5 * (3 + 2); // Addieren wir die Werte,die in den Klammern sind = 5 und dann 5*5= 25
	a *= 5 + 5; // Vor der Zuweisung addieren wir 5+5 =10 und dann machen wir weiter :  *= hei�t a= a*10 d.h die Wert von oben(25) * 10= 250
	a %= 2 * 3; // Die Multiplikation hat die Priorit�t (2*3 =5) dann a = 250 %6 --> 4
	a = !(--b == 0); // (--b = 2-1) d.h (1 == 0) das ist falsch (0) !(0) = 1
	a = 0 && 0 + 2; // Wie addieren zu erst 0+2 : 2 ist Wahr und dann (0 && 2) ist falsch(0) 
	a = b++ * 2; // b = 1 --> 1*2 =2 und dann b = 1+1=2
	a = -5 - 5; // a = -10
	a = -(+b++); // b = 2 ; a = -(+2) ergibt -2 und dann b = 2+1 =3 
	a = 5 == 5 && 0 || 1; // (5 == 5) ist Wahr(1) --> (1 && 0) ist (0) --> (0 || 1) ist (1) a = 1
	a = ((((((b + b) * 2) + b) && b || b)) == b); /*beginnen wir von (b+b) --> (3+3 =6) --> (6*2 = 12) -->(12+3= 15) --> (15(wahr) && 3(wahr) =(1))
	 --> (1 || 3 = wahr (1)) --> 1 ==b(3)? false (0) ... a = 0*/
	a = b + ++b; // ++b hat die Priorit�t (3+1 = 4) dann ist auch die erste b 4 geworden --> 4+4= 8
	a = sizeof(int) * sizeof(a);  //sizeof(int) =4 Byte ... sizeof(a)[a ist auch int] = 4Byte ---> 4*4 =16

	return 0;

}