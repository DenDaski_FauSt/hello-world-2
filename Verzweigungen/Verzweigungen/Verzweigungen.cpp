#include<iostream>
using namespace std;

int main()
{
	int i, j;

	i = 0; j = -5;
	if (i++ || j++) ++i; /* [i = 0 (falsch) ||  j = -5 (wahr)] = wahr(1) dann werden alle Inkrementierungen durchgef�hrt
	i++, j++, ++i --> i=2 , j =-4*/
	cout << i << ',' << j << endl;

	i = 1; j = -5;
	if (i++ || j++) ++i; /* [i = 1 (whar) und wir gehen zu der zweiten Bedingung nicht, weil wenn die erste Bedingung bei ODER-Operarot
	wahr ist ,wird die zweite Bedingung ignoriert] = wahr(1) dann wird nur (i) Inkrementiert
	i++, ++i --> i=3 , j =-5*/
	cout << i << ',' << j << endl;

	i = 0; j = -5;
	if (i++ && j++) ++i; /* [i = 0 (falsch) und wir gehen zu der zweiten Bedingung nicht, weil wenn die erste Bedingung bei AND-Operarot
	falsch ist , wird die zweite Bedingung auch ignoriert] = falsch(0) d.h die zweite Inkrementierung wird nicht gemacht(++i)
	und (i) wird in diesem Fall nur einmal Inkrementiert
	i++ --> i=1 , j =-5*/
	cout << i << ',' << j << endl;

	i = 1; j = -5;
	if (i++ && j++) ++i; /* [i = 1 (wahr) && j=-5(wahr)] = alle Inkrementierungen werden durhcgef�hrt
	i++, ++i --> i=3 , j =-4*/
	cout << i << ',' << j << endl;
}
