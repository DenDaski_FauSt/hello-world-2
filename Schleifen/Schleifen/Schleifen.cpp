#include<iostream>
using namespace std;

int main()
{
	int i, k, n;

	i = 101, k = 5, n = 10;
	while (i > 0) k = 2 * k; // Die Schleife stoppt wenn i <= 0 ist .Aber i wird nicht dekrementiert und die Schlie wird bis unendlich ausgeführt

	i = 101, k = 5, n = 10;
	while (i != 0) i = i - 2; // Die Schleife stoppt wenn i = 0 ist. (i = 101) wenn wie immer -2 von i subtrahieren, werden wir niemals zu 0 kommen.die Schlie wird bis unendlich ausgeführt

	i = 101, k = 5, n = 10;
	while (n != i) /*Die Schleife stoppt wenn n = i ist.n = 2 * i und i = 101. i wird inkrementiert und n wird mit 2 und i multiplitziert,
				   n = 204, i = 102 dann n = 206, i = 103. Das wird sich bis unendlich wiederhölt*/
	{
		++i;
		n = 2 * i;
	}

}
